extends Area2D

var mMouseIsHere = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _on_Area2D_mouse_entered():
	mMouseIsHere = true

func _on_Area2D_mouse_exited():
	mMouseIsHere = false

func IsMouseHere():
	return mMouseIsHere
