extends Node2D

##### Main Menu
func initiateMainMenuMusic():
	var musicPlayer = AudioStreamPlayer.new()
	self.add_child(musicPlayer)
	musicPlayer.stream = load("res://assets/SFX/main-menu.ogg")
	musicPlayer.play()	

#### Combine
func playCombineSound():
	var sfxPlayer = AudioStreamPlayer.new()
	self.add_child(sfxPlayer)
	sfxPlayer.stream = load("res://assets/SFX/reconstruct_object_2.wav")
	sfxPlayer.play()

#### Move
func playMoveSound():
	var sfxPlayer = AudioStreamPlayer.new()
	self.add_child(sfxPlayer)
	sfxPlayer.stream = load("res://assets/SFX/moving_item.wav")
	sfxPlayer.play()

#### Cat Meows
func playRandomCatSound():
	var sfxPlayer = AudioStreamPlayer.new()
	self.add_child(sfxPlayer)
	var randSoundNb = rand_range(1, 2)
	if randSoundNb == 1:
		sfxPlayer.stream = load("res://assets/SFX/cat_meow_1.wav")
	else:
		sfxPlayer.stream = load("res://assets/SFX/cat_meow_2.wav")
	sfxPlayer.play()
