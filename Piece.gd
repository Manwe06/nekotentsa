extends Area2D

const SpritesheetFrameSize = Vector2(96.0, 96.0) * 3

var mPieceConfig = null
var mPartOffsets = []

var mBaseSlotPosition = Vector2(0, 0)
var mX = 0
var mY = 0
var mIsGrabbed = false
var mGrabOffset = Vector2()
var mGhostPosition = Vector2()
var mGhostX = 0
var mGhostY = 0
var mIsOverRepairArea = false
var mIsToBeRemoved = false
var mSlotsHovered = []
var mDroppedOverPiece = null


func Init(pieceConfig, slotSize):
	mPieceConfig = pieceConfig
	mPartOffsets = pieceConfig.partOffsets
	
	var sprite = get_node("Sprite")
	sprite.frame = pieceConfig.frameIndex
	sprite.scale = (slotSize * 3) / SpritesheetFrameSize;
	
	for partOffset in mPartOffsets:
		var collision = CollisionShape2D.new()
		add_child(collision)
		var shape = RectangleShape2D.new()
		shape.extents = slotSize / 2.0
		collision.position = partOffset * slotSize + slotSize / 2.0
		collision.shape = shape
	
	self.connect("mouse_entered", self, "_on_Piece_mouse_entered")
	self.connect("mouse_exited", self, "_on_Piece_mouse_exited")

func GetConfig():
	return mPieceConfig

func GetOffsets():
	return mPartOffsets

func IsGrabbed():
	return mIsGrabbed

func SetBaseSlotPosition(p, x, y):
	mBaseSlotPosition = p
	mX = x
	mY = y

func GetBaseSlotPosition():
	return mBaseSlotPosition

func GetX():
	return mX

func GetY():
	return mY

func GetGhostPosition():
	return mGhostPosition

func GetGhostX():
	return mGhostX

func GetGhostY():
	return mGhostY

func SetIsOverRepairArea(isThere):
	mIsOverRepairArea = isThere

func IsToBeRemoved():
	return mIsToBeRemoved

func GetDroppedOverPiece():
	return mDroppedOverPiece

func ClearDroppedOverPiece():
	mDroppedOverPiece = null

func SetSlotsHovered(s):
	mSlotsHovered = s

var soundPlayer = null

func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton:
		var wasGrabbed = mIsGrabbed
		mIsGrabbed = event.pressed
		mGrabOffset = get_parent().position - get_global_mouse_position()
		
		if not wasGrabbed and mIsGrabbed:
			mGhostPosition = get_parent().position
			mGhostX = mX
			mGhostY = mY
		
		if wasGrabbed and not mIsGrabbed:
			mDroppedOverPiece = null
			var isValid = len(mSlotsHovered) > 0
			for slotScript in mSlotsHovered:
				var droppedOverPiece = slotScript.GetBlockedBy()
				if slotScript.IsBlocked() and droppedOverPiece != self:
					isValid = false
					mDroppedOverPiece = droppedOverPiece
					break;
			mSlotsHovered.clear()
			
			if mIsOverRepairArea:
				mIsToBeRemoved = true
			else:
				if isValid:
					get_parent().position = mBaseSlotPosition
				else:
					if mDroppedOverPiece == null:
						get_parent().position = mGhostPosition
			## Sound for drop
			if soundPlayer == null:
				soundPlayer = AudioStreamPlayer.new()
				self.add_child(soundPlayer)
			soundPlayer.stream = load("res://assets/SFX/moving_item.wav")
			soundPlayer.play()

func _process(delta):
	if Input.is_mouse_button_pressed(BUTTON_LEFT) and mIsGrabbed:
		get_parent().position = get_global_mouse_position() + mGrabOffset

func _on_Piece_mouse_entered():
	pass

func _on_Piece_mouse_exited():
	pass
