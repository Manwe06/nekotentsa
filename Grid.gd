extends Node2D

const PieceTexture = preload("res://icon.png")

const SlotScene = preload("EmptySlot.tscn")
const PieceScene = preload("Piece.tscn")
const GridSize = Vector2(9, 5)
const SlotSize = Vector2(96, 96)
const FallSpeed = 7.5

class PieceMember:
	var offset = Vector2()
	var matchIndex = -1
	var otherIndices = []
	
	func _init(o, mi, oi):
		offset = o
		matchIndex = mi
		otherIndices = oi
	
class PieceConfig:
	var thisIndex = 0
	var frameIndex = 0
	var partOffsets = []
	var members = []
	
	func _init(ti, fi, po, m):
		thisIndex = ti
		frameIndex = fi
		partOffsets = po
		members = m

var PieceConfigs = [
	PieceConfig.new(0, 0, [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2)], [  # ALEX check this
		PieceMember.new(Vector2(0, 0), 1, []),
		PieceMember.new(Vector2(0, 1), 2, []),
		PieceMember.new(Vector2(0, 2), 3, [])
	]),
	PieceConfig.new(1, 1, [Vector2(0, 0)], []),
	PieceConfig.new(2, 2, [Vector2(0, 0)], []),
	PieceConfig.new(3, 3, [Vector2(0, 0)], []),
	
	PieceConfig.new(4, 4, [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2)], [
		PieceMember.new(Vector2(0, 0), 5, []),
		PieceMember.new(Vector2(0, 1), 6, []),
		PieceMember.new(Vector2(0, 2), 7, [])
	]),
	PieceConfig.new(5, 5, [Vector2(0, 0)], []),
	PieceConfig.new(6, 6, [Vector2(0, 0)], []),
	PieceConfig.new(7, 7, [Vector2(0, 0)], []),
	
	PieceConfig.new(8, 8, [Vector2(0, 0), Vector2(0, 1), Vector2(0, 2)], [
		PieceMember.new(Vector2(0, 0), 9, []),
		PieceMember.new(Vector2(0, 1), 10, []),
		PieceMember.new(Vector2(0, 2), 11, [])
	]),
	PieceConfig.new(9, 9, [Vector2(0, 0)], []),
	PieceConfig.new(10, 10, [Vector2(0, 0)], []),
	PieceConfig.new(11, 11, [Vector2(0, 0)], []),
	
	PieceConfig.new(12, 12, [Vector2(0, 0), Vector2(0, 1), Vector2(1, 0), Vector2(1, 1), Vector2(2, 0), Vector2(2, 1)], [
		PieceMember.new(Vector2(0, 0), 13, []),
		PieceMember.new(Vector2(1, 0), 14, []),
		PieceMember.new(Vector2(2, 0), 15, [])
	]),
	PieceConfig.new(13, 13, [Vector2(0, 0), Vector2(0, 1)], []),
	PieceConfig.new(14, 14, [Vector2(0, 0), Vector2(0, 1)], []),
	PieceConfig.new(15, 15, [Vector2(0, 0), Vector2(0, 1)], []),
	
	PieceConfig.new(16, 16, [Vector2(0, 0), Vector2(0, 1), Vector2(1, 0), Vector2(1, 1), Vector2(2, 0), Vector2(2, 1)], [
		PieceMember.new(Vector2(0, 0), 17, []),
		PieceMember.new(Vector2(1, 0), 18, []),
		PieceMember.new(Vector2(2, 0), 19, [])
	]),
	PieceConfig.new(17, 17, [Vector2(0, 0), Vector2(0, 1)], []),
	PieceConfig.new(18, 18, [Vector2(0, 0), Vector2(0, 1)], []),
	PieceConfig.new(19, 19, [Vector2(0, 0), Vector2(0, 1)], []),
	
	PieceConfig.new(20, 20, [Vector2(0, 0), Vector2(0, 1), Vector2(1, 0), Vector2(1, 1), Vector2(2, 0), Vector2(2, 1)], [
		PieceMember.new(Vector2(0, 0), 21, []),
		PieceMember.new(Vector2(1, 0), 22, []),
		PieceMember.new(Vector2(2, 0), 23, [])
	]),
	PieceConfig.new(21, 21, [Vector2(0, 0), Vector2(0, 1)], []),
	PieceConfig.new(22, 22, [Vector2(0, 0), Vector2(0, 1)], []),
	PieceConfig.new(23, 23, [Vector2(0, 0), Vector2(0, 1)], []),
	
	PieceConfig.new(24, 24, [Vector2(0, 0), Vector2(0, 1), Vector2(1, 0), Vector2(1, 1)], [
		PieceMember.new(Vector2(0, 0), 25, []),
		PieceMember.new(Vector2(0, 1), 26, [])
	]),
	PieceConfig.new(25, 25, [Vector2(0, 0), Vector2(1, 0)], []),
	PieceConfig.new(26, 26, [Vector2(0, 0), Vector2(1, 0)], []),
	
	PieceConfig.new(27, 28, [Vector2(0, 0), Vector2(0, 1), Vector2(1, 0), Vector2(1, 1)], [
		PieceMember.new(Vector2(0, 0), 28, []),
		PieceMember.new(Vector2(0, 1), 29, [])
	]),
	PieceConfig.new(28, 29, [Vector2(0, 0), Vector2(1, 0)], []),
	PieceConfig.new(29, 30, [Vector2(0, 0), Vector2(1, 0)], []),
	
	PieceConfig.new(30, 32, [Vector2(0, 0), Vector2(0, 1), Vector2(1, 0), Vector2(1, 1)], [
		PieceMember.new(Vector2(0, 0), 31, []),
		PieceMember.new(Vector2(0, 1), 32, [])
	]),
	PieceConfig.new(31, 33, [Vector2(0, 0), Vector2(1, 0)], []),
	PieceConfig.new(32, 34, [Vector2(0, 0), Vector2(1, 0)], [])
]
var CombinedSets = []

var mNextSetToDrop = null
var slots = []
var slotsXY = []
var pieces = []

func _init():
	randomize()
	
	slotsXY = []
	for x in range(GridSize.x):
		var slotsY = []
		for y in range(GridSize.y):
			var slot = SlotScene.instance()
			slot.position = Vector2(x, y) * SlotSize
			add_child(slot)
			slots.append(slot)
			slot.get_node("Area2D").Init(x, y)
			slotsY.append(slot)
		slotsXY.append(slotsY)
	
	for pieceConfig in PieceConfigs:
		if len(pieceConfig.members) > 0:
			CombinedSets.append(pieceConfig);
	
	#MakePiece(PieceConfigs[0], Vector2(0, 0))
	#MakePiece(PieceConfigs[1], Vector2(3 * 96, 0))
	#MakePiece(PieceConfigs[2], Vector2(5 * 96, 0))
	#MakePiece(PieceConfigs[3], Vector2(7 * 96, 0))
	
	#MakePiece(PieceConfigs[30], Vector2(0, 0))
	#MakePiece(PieceConfigs[31], Vector2(3 * 96, 0))
	#MakePiece(PieceConfigs[32], Vector2(5 * 96, 0))
	#MakePiece(PieceConfigs[23], Vector2(7 * 96, 0))

class SequencePart:
	var delay = 0.0
	var setIndex = -1 # random
	var spawnWholeSet = false
	
	func _init(d, si, sws):
		delay = d
		setIndex = si
		spawnWholeSet = sws

var mSequenceNextIndex = 0
var mSequence = [ # ALEX check this
	SequencePart.new(6.0, 24, true), # full plate
	SequencePart.new(6.0, 12, true), # full bowl
	SequencePart.new(4.0,  0, true), # full vase
	
	SequencePart.new(3.0,  4, true), # plate
	SequencePart.new(5.0,  4, false), # bottle
	SequencePart.new(3.0,  27, true), # plate
	SequencePart.new(6.0,  16, false), # bowl
	
	SequencePart.new(2.0,  27, false), # plate
	SequencePart.new(5.0,  24, true), # plate
	SequencePart.new(4.0,  0, true), # bottle
	SequencePart.new(6.0,  16, false), # bowl
	# Intensify
	SequencePart.new(2.0,  8, false), # bottle
	SequencePart.new(3.0,  16, false), # bowl
	SequencePart.new(2.0,  30, true), # plate
	SequencePart.new(4.0,  24, false), # plate
	SequencePart.new(8.0, 16, true), # bowl
	
	SequencePart.new(2.0, -1, false), # random !!!
	SequencePart.new(3.0, -1, false), # random !!!
	SequencePart.new(2.0, -1, true), # random !!!
	SequencePart.new(7.0, -1, false), # random !!!
	
	SequencePart.new(3.0,  0, false), # bottle
	SequencePart.new(2.0,  12, true), # bowl
	SequencePart.new(4.0,  30, false), # plate
	SequencePart.new(7.0,  27, false), # plate
	
	SequencePart.new(2.0, -1, false), # random !!!
	SequencePart.new(4.0, -1, true), # random !!!
	SequencePart.new(2.0, -1, false), # random !!!
	SequencePart.new(4.0, -1, false), # random !!!
	SequencePart.new(2.0, -1, false), # random !!!
	
	
	SequencePart.new(3.5, -1, false), # random !!!
]
var mSequenceOptions = mSequence[0]

var mNextPieceIndex = 0
var mNextPieceNames = [
	"./NextPiece0",
	"./NextPiece1",
	"./NextPiece2"
]
var mNextNode = null
var mScore = 0

var canAnimationIndex = 0
var catAnimations = ["Meow", "Blinking", "Idle", "Laughing", "Push"]
	
func PickNextSetToDrop():
	mSequenceOptions = mSequence[mSequenceNextIndex]
	if mSequenceNextIndex + 1 < len(mSequence):
		mSequenceNextIndex += 1
	
	for name in mNextPieceNames:
		get_node(name).visible = false
	
	mNextNode = get_node(mNextPieceNames[mNextPieceIndex])
	mNextNode.visible = true
	
	var cat = get_node("CatNode")
	var animator = cat.get_node("CatAnimator")
	animator.current_animation = catAnimations[canAnimationIndex]
	if canAnimationIndex == 0:
		var musicPlayer = AudioStreamPlayer.new()
		self.add_child(musicPlayer)
		musicPlayer.stream = load("res://assets/SFX/cat_meow_1.wav")
		musicPlayer.play()
	if canAnimationIndex == 3:
		var musicPlayer = AudioStreamPlayer.new()
		self.add_child(musicPlayer)
		musicPlayer.stream = load("res://assets/SFX/cat_mocking.wav")
		musicPlayer.play()
	canAnimationIndex = (canAnimationIndex + 1) % len(catAnimations)
	
	if mSequenceOptions.setIndex > -1:
		mNextSetToDrop = PieceConfigs[mSequenceOptions.setIndex]
		#print("#", mSequenceOptions.setIndex, " mNextSetToDrop = ", len(mNextSetToDrop.members))
	else: # random
		var i = randi() % len(CombinedSets)
		mNextSetToDrop = CombinedSets[i]
	
	mNextNode.frame = mNextSetToDrop.frameIndex
	
	mNextPieceIndex = (mNextPieceIndex + 1) % len(mNextPieceNames)

func MakePiece(config, atPosition):
	var newPiece = PieceScene.instance()
	add_child(newPiece)
	pieces.append(newPiece)
	newPiece.get_node("Area2D").Init(config, SlotSize)
	newPiece.position = atPosition

func FindNearestSlot(p):
	var bestSlot = null
	var bestDistance = 10000000.0
	for slot in slots:
		var distance = slot.position.distance_to(p)
		if bestDistance > distance:
			bestDistance = distance
			bestSlot = slot
	return bestSlot

class PlacedPiece:
	var pieceScript = null
	var bottomYs = {}

func _ready():
	PickNextSetToDrop()
	
	## Start the music
	var musicPlayer = AudioStreamPlayer.new()
	self.add_child(musicPlayer)
	musicPlayer.stream = load("res://assets/SFX/gameplay_ambient.wav")
	musicPlayer.play()

func _process(delta):
	if mNextNode != null:
		var cat = get_node("CatNode")
		var animator = cat.get_node("CatAnimator")
		var targetPosition = mNextNode.position# + Vector2(0, 300)
		var distanceToCat = cat.position.distance_to(targetPosition)
		if distanceToCat > 0.01:
			#animator.current_animation = "Meow"
			var speed = 5.0
			var toNext = (targetPosition - cat.position).normalized()
			if distanceToCat > speed:
				cat.position += toNext * speed
			else:
				cat.position = targetPosition
		else:
			animator.current_animation = "Hand"
	
	if mFlyingPiece != null:
		var target = self.position + Vector2((GridSize.x / 2.0) * SlotSize.x, -150)
		var distanceToTarget = mFlyingPiece.position.distance_to(target)
		var toTarget = (target - mFlyingPiece.position).normalized()
		var speed = 10.0
		if distanceToTarget > speed:
			mFlyingPiece.position += toTarget * speed
		else: # reach it!
			mFlyingPiece.visible = false
			mFlyingPiece = null
	
	for piece in pieces: # swaps and restores
		var pieceScript = piece.get_node("Area2D")
		var droppedOverPiece = pieceScript.GetDroppedOverPiece()
		if droppedOverPiece != null:
			var iCanGoThere = CanPlaceAt(pieceScript.GetConfig(), droppedOverPiece.GetX(), droppedOverPiece.GetY(), droppedOverPiece)
			var itCanComeHere = CanPlaceAt(droppedOverPiece.GetConfig(), pieceScript.GetGhostX(), pieceScript.GetGhostY(), pieceScript)
			if iCanGoThere and itCanComeHere:
				#print("Valid Swap!")
				var temp = pieceScript.GetGhostPosition()
				piece.position = droppedOverPiece.get_parent().position
				droppedOverPiece.get_parent().position = temp
			else: # revert to ghost position
				piece.position = pieceScript.GetGhostPosition()
			pieceScript.ClearDroppedOverPiece()
	
	for slot in slots:
		slot.get_node("Area2D").Clear()
	
	var repairedArea = get_node("./RepairedArea/Area2D")
	
	var placedPieces = []
	
	for piece in pieces:
		var pieceScript = piece.get_node("Area2D")
		var origin = piece.position
		var originGhost = pieceScript.GetGhostPosition()
		var originSlot = null
		var originSlotScript = null
		var slotsOccupied = []
		var slotsHovered = []
		var offsets = pieceScript.GetOffsets()
		
		for offset in offsets:
			if not pieceScript.IsGrabbed():
				var slot = FindNearestSlot(origin + offset * SlotSize)
				var slotScript = slot.get_node("Area2D")
				slotScript.SetBlockedBy(pieceScript)
				if originSlot == null:
					originSlot = slot
					originSlotScript = slotScript
				if not slotsOccupied.has(slotScript):
					slotsOccupied.append(slotScript)
			else: # dragging
				var slot = FindNearestSlot(originGhost + offset * SlotSize)
				var slotScript = slot.get_node("Area2D")
				slotScript.SetBlockedBy(pieceScript)
				if not slotsOccupied.has(slotScript):
					slotsOccupied.append(slotScript)
				
				var slotHover = FindNearestSlot(origin + offset * SlotSize)
				var slotHoverScript = slotHover.get_node("Area2D")
				slotHoverScript.SetHovered()
				if originSlot == null:
					originSlot = slotHover
					originSlotScript = slotHoverScript
				if not slotsHovered.has(slotHoverScript):
					slotsHovered.append(slotHoverScript)
		
		if len(slotsOccupied) == len(offsets):
			pieceScript.SetBaseSlotPosition(originSlot.position, originSlotScript.GetX(), originSlotScript.GetY())
		
		if len(slotsHovered) == len(offsets):
			pieceScript.SetSlotsHovered(slotsHovered)
		else:
			pieceScript.SetSlotsHovered([])
		
		var bottomYs = {}
		for slotScript in slotsOccupied:
			var x = slotScript.GetX()
			var y = slotScript.GetY()
			if bottomYs.has(x):
				if bottomYs[x] < y:
					bottomYs[x] = y
			else:
				bottomYs[x] = y
		
		if len(pieceScript.GetConfig().members) > 0:
			pieceScript.SetIsOverRepairArea(repairedArea.IsMouseHere())
		else:
			pieceScript.SetIsOverRepairArea(false)
		
		if pieceScript.IsToBeRemoved():
			for slotScript in slotsOccupied:
				slotScript.Clear()
			var pieceParent = pieceScript.get_parent()
			pieces.remove(pieces.find(pieceParent))
			remove_child(pieceParent)
			# SCORE
			mScore += 50 * len(slotsOccupied)
			get_node("Score").text = str(mScore)
			
			var musicPlayer = AudioStreamPlayer.new()
			self.add_child(musicPlayer)
			musicPlayer.stream = load("res://assets/SFX/repaired_item.wav")
			musicPlayer.play()
			continue
		
		var placedPiece = PlacedPiece.new()
		placedPiece.pieceScript = pieceScript
		placedPiece.bottomYs = bottomYs
		placedPieces.append(placedPiece)
	
	for placedPiece in placedPieces:
		var pieceScript = placedPiece.pieceScript
		var bottomYs = placedPiece.bottomYs
		
		var canFall = true
		for x in bottomYs.keys():
			var y = bottomYs[x]
			if y + 1 >= GridSize.y or slotsXY[x][y + 1].get_node("Area2D").IsBlocked():
				canFall = false
				break
		
		if not pieceScript.IsGrabbed():
			var piece = pieceScript.get_parent()
			var baseY = pieceScript.GetBaseSlotPosition().y
			if canFall:
				piece.position += Vector2(0, FallSpeed)
			elif piece.position.y < baseY:
				var remaining = baseY - piece.position.y
				var speed = FallSpeed
				if remaining < speed:
					speed = remaining
				piece.position += Vector2(0, speed)
	
	####### COMBINE THEM #######################################################
	var foundFirstMatch = false # prevent more than one creation per frame, because HACKSSSS!!!
	for combinedSet in CombinedSets:
		if foundFirstMatch:
			break
		for x in range(GridSize.x):
			if foundFirstMatch:
				break
			for y in range(GridSize.y):
				if foundFirstMatch:
					break
				
				var membersFound = []
				var foundAtSlots = []
				
				for member in combinedSet.members:
					var mx = x + int(member.offset.x)
					var my = y + int(member.offset.y)
					if mx >= 0 and mx < GridSize.x and my >= 0 and my < GridSize.y:
						var slotScript = slotsXY[mx][my].get_node("Area2D")
						var pieceScript = slotScript.GetBlockedBy()
						if pieceScript != null:
							if pieceScript.GetConfig().thisIndex == member.matchIndex:
								if pieceScript.GetBaseSlotPosition().distance_to(slotScript.get_parent().position) < 0.1:
									membersFound.append(pieceScript)
									foundAtSlots.append(slotScript)
				
				if len(membersFound) == len(combinedSet.members):
					foundFirstMatch = true
					#print("MATCH")
					
					var originPosition = membersFound[0].GetBaseSlotPosition()
					
					for slotScript in foundAtSlots:
						slotScript.Clear()
					
					for pieceScript in membersFound:
						var piece = pieceScript.get_parent()
						pieces.remove(pieces.find(piece))
						remove_child(piece)
					
					MakePiece(combinedSet, originPosition)
					
					get_parent().playCombineSound()
	
	############################################################################
	for slot in slots:
		slot.get_node("Area2D").Update(delta)

func CanPlaceAt(config, x, y, ignorePieceScript):
	for offset in config.partOffsets:
		var mx = x + int(offset.x)
		var my = y + int(offset.y)
		if mx >= 0 and mx < GridSize.x and my >= 0 and my < GridSize.y:
			var slotScript = slotsXY[mx][my].get_node("Area2D")
			if slotScript.IsBlocked():
				if ignorePieceScript == null:
					return false
				if slotScript.GetBlockedBy() != ignorePieceScript:
					return false
		else:
			return false
	return true

func FindWhereToSpawnPieces(configs):
	var spawnPoints = []
	
	var firstRow = []
	for x in range(GridSize.x):
		if not slotsXY[x][0].get_node("Area2D").IsBlocked():
			firstRow.append(x)
	
	for config in configs:
		#var j = randi() % int(GridSize.x)
		#var spawnPoint = Vector2(j * SlotSize.x, 0)
		#spawnPoints.append(spawnPoint)
		var width = 1
		for offset in config.partOffsets:
			width = max(width, offset.x + 1)
		
		var goodXs = []
		for x in firstRow:
			if CanPlaceAt(config, x, 0, null):
				if width == 2: # terrible HACK number 5
					if firstRow.has(x + 1):
						goodXs.append(x)
				else:
					goodXs.append(x)
		
		if len(goodXs) <= 0:
			return []
		
		var goodX = goodXs[randi() % len(goodXs)]
		spawnPoints.append(Vector2(goodX * SlotSize.x, 0))
		
		var toErase = []
		var first = firstRow.find(goodX)
		var last = first + width
		
		while first < last:
			toErase.append(firstRow[first])
			first += 1
			
		for t in toErase:
			firstRow.erase(t)
	
	return spawnPoints

var mFlyingPiece = null
const FlyTime = 1.0
func _on_TimerFlyToGrid_timeout():
	if mNextNode != null:
		mFlyingPiece = get_node("FlyingPiece")
		mFlyingPiece.visible = true
		mFlyingPiece.position = mNextNode.position
		#mFlyingPiece.offset = Vector2(0, 0)
		mFlyingPiece.frame = mNextNode.frame
		
		mNextNode.visible = false

var mMissingPieces = []

func _on_Timer_timeout():
	var spawnTimer = get_node("Timer")
	spawnTimer.set_wait_time(mSequenceOptions.delay)
	spawnTimer.set_one_shot(true)
	spawnTimer.start()
	
	var flyTimeClamped = mSequenceOptions.delay - FlyTime
	if flyTimeClamped < 0.0:
		flyTimeClamped = 0.01
	var flyToGridTimer = get_node("TimerFlyToGrid")
	flyToGridTimer.set_wait_time(flyTimeClamped)
	flyToGridTimer.set_one_shot(true)
	flyToGridTimer.start()
	#print("SPAWN!")
	
	var configsToSpawn = []
	for member in mNextSetToDrop.members:
		var config = PieceConfigs[member.matchIndex]
		configsToSpawn.append(config)
	
	if not mSequenceOptions.spawnWholeSet:
		var index = randi() % len(configsToSpawn)
		mMissingPieces.append(configsToSpawn[index])
		
		if len(mMissingPieces) > 1:
			var previous = mMissingPieces[randi() % len(mMissingPieces)]
			configsToSpawn[index] = previous
			mMissingPieces.erase(previous)
		else:
			var anotherSet = CombinedSets[randi() % len(CombinedSets)]
			configsToSpawn[index] = PieceConfigs[anotherSet.members[0].matchIndex]
	
	var spawnPoints = FindWhereToSpawnPieces(configsToSpawn)
	if len(spawnPoints) <= 0:
		print("GAME OVER!")
		get_tree().change_scene("res://GameOver.tscn")
		return
	
	var musicPlayer = AudioStreamPlayer.new()
	self.add_child(musicPlayer)
	musicPlayer.stream = load("res://assets/SFX/breaking_porcelain.wav")
	musicPlayer.play()
	
	for i in range(len(spawnPoints)):
		MakePiece(configsToSpawn[i], spawnPoints[i])
	
	PickNextSetToDrop()
