extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var musicPlayer = AudioStreamPlayer.new()
	self.add_child(musicPlayer)
	musicPlayer.stream = load("res://assets/SFX/main-menu.wav")
	musicPlayer.play()
	
	var sfxPlayer = AudioStreamPlayer.new()
	self.add_child(sfxPlayer)
	sfxPlayer.stream = load("res://assets/SFX/cat_mocking.wav")
	sfxPlayer.play()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed:
		get_tree().change_scene("res://root.tscn")
