extends Area2D

const NeutralTexture = preload("res://assets/slot.png")
const HoverTexture = preload("res://assets/slot_hover.png")
const PressTexture = preload("res://assets/slot_press.png")

var mIsHovered = false
var mIsBlockedBy = null
var mX = 0
var mY = 0

func _on_Area2D_mouse_entered():
	pass
	#if selected:
	#	get_node("background").texture = PressTexture
	#else:
	#	get_node("background").texture = HoverTexture

func _on_Area2D_mouse_exited():
	pass
	#if not selected:
	#	get_node("background").texture = NeutralTexture

func _on_Area2D_input_event(viewport, event, shape_idx):
	pass
	#if event is InputEventMouseButton:
	#	if event.button_index == BUTTON_LEFT and event.pressed:
	#		selected = not selected
	#		if selected:
	#			print("now selected")
	#			get_node("background").texture = PressTexture
	#		else:
	#			print("now not selected")
	#			get_node("background").texture = HoverTexture

func Init(x, y):
	mX = x
	mY = y

func GetX():
	return mX

func GetY():
	return mY

func Clear():
	mIsBlockedBy = null
	mIsHovered = false

func SetBlockedBy(byPiece):
	mIsBlockedBy = byPiece

func IsBlocked():
	return mIsBlockedBy != null

func GetBlockedBy():
	return mIsBlockedBy

func SetHovered():
	mIsHovered = true

func Update(delta):
	if IsBlocked():
		get_node("background").texture = PressTexture
	elif mIsHovered:
		get_node("background").texture = HoverTexture
	else:
		get_node("background").texture = NeutralTexture
